package com.hcmus.group20.taskman.services;

import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.hcmus.group20.taskman.models.MemberModel;
import com.hcmus.group20.taskman.models.TaskStorageMetadata;

/**
 * Created by nglekhoi on 30-Jan-17.
 */

public class LoginService {
    private DatabaseReference mDatabase;

    public LoginService() {
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }

    public void EnsurePersonalStorage(final String email, final String userUid) {
        // Get Member data first;
        Query mQueryRef = mDatabase.child("Members").orderByKey().equalTo(userUid);

        mQueryRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) {
                    // Member not existed
                    MemberModel newMember = new MemberModel();
                    // Create new personal storage
                    TaskStorageMetadata storageMetadata = new TaskStorageMetadata();
                    storageMetadata.Name = email;
                    storageMetadata.Description = email + " storage";
                    // Get storage key
                    String storageKey = mDatabase.child("TaskStorageMetadatas").push().getKey();
                    mDatabase.child("TaskStorageMetadatas").child(storageKey).setValue(storageMetadata);
                    // Assign storage key and email to member
                    newMember.PersonalStorageId = storageKey;
                    newMember.Email = email;
                    mDatabase.child("Members").child(userUid).setValue(newMember);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
    }
}
