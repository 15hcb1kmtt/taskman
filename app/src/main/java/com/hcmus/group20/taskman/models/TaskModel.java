package com.hcmus.group20.taskman.models;

import java.util.Date;

/**
 * Created by nglekhoi on 30-Jan-17.
 */

public class TaskModel {
    public String Title;
    // For reading and saving Date, read: http://stackoverflow.com/questions/37976468/saving-and-retrieving-date-in-firebase
    public long FromDate;
    public long ToDate;
    public String Description;
    public String CreatedById;
    public String CreatedByEmail;
    public String AssignToId;
    public String AssignToEmail;
    public String Priority;
    public String Status; // Open, InProgress, Completed, Obsolete
}
