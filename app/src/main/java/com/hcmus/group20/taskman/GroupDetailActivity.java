package com.hcmus.group20.taskman;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.ViewGroup;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.hcmus.group20.taskman.models.AssignToModel;
import com.hcmus.group20.taskman.models.GroupMemberModel;
import com.hcmus.group20.taskman.models.MemberModel;
import com.hcmus.group20.taskman.models.TaskStorageMetadata;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GroupDetailActivity extends AppCompatActivity {
    private GroupMemberDataAdapter adapter;

    @BindView(R.id.group_detail_member_list)
    public RecyclerView recyclerView;

    private String groupId;
    private String groupName;
    private String userId;
    private Boolean isAdmin;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_detail);
        groupId = getIntent().getStringExtra("GroupId");
        groupName = getIntent().getStringExtra("GroupName");
        userId = getIntent().getStringExtra("UserId");
        isAdmin = getIntent().getBooleanExtra("IsAdmin", Boolean.FALSE);
        ButterKnife.bind(this);
        initViews();
    }

    @OnClick(R.id.group_detail_btn_add_member)
    public void AddMember() {
        FragmentManager fm = getSupportFragmentManager();
        AddMemberDialog editNameDialogFragment = AddMemberDialog.newInstance("Nhập Email", groupId, groupName);
        editNameDialogFragment.show(fm, "fragment_edit_name");
    }

    @OnClick(R.id.group_detail_btn_delete_group)
    public void DeleteGroup() {

    }

    @OnClick(R.id.group_detail_btn_leave_group)
    public void LeaveGroup() {

    }

    public void RemoveMember(String memberId) {
        final DatabaseReference mDbReference = FirebaseDatabase.getInstance().getReference();
        mDbReference.child("Members").child(memberId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    MemberModel member = dataSnapshot.getValue(MemberModel.class);
                    String memberKey = dataSnapshot.getKey();
                    RemoveMemberFromGroup(member, memberKey, mDbReference);
                    Toast.makeText(GroupDetailActivity.this, "Xóa thành viên thành công.", Toast.LENGTH_SHORT).show();
                    PopulateMembers();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
    }

    private void RemoveMemberFromGroup(final MemberModel member, final String memberKey, final DatabaseReference mDatabase) {
        member.MemberOfGroups.remove(groupId);
        mDatabase.child("TaskStorageMetadatas")
                .child(groupId)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            TaskStorageMetadata meta = dataSnapshot.getValue(TaskStorageMetadata.class);
                            if (meta.Members != null) {
                                meta.Members.remove(memberKey);
                            }
                            mDatabase.child("TaskStorageMetadatas").child(groupId).setValue(meta);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    }
                );
        mDatabase.child("Members").child(memberKey).setValue(member);
    }

    private void initViews(){
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        PopulateMembers();
    }

    private void PopulateMembers() {
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        database.getReference("TaskStorageMetadatas")
                .child(groupId).child("Members")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    if (snapshot.exists()) {
                        List<GroupMemberModel> items = new ArrayList<GroupMemberModel>();
                        for (DataSnapshot member : snapshot.getChildren()) {
                            GroupMemberModel memberData = new GroupMemberModel();
                            memberData.Id = member.getKey();
                            memberData.Email = member.getValue().toString();
                            items.add(memberData);
                        }

                        adapter = new GroupMemberDataAdapter(GroupDetailActivity.this, items, userId, isAdmin);
                        recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
        });
    }
}
