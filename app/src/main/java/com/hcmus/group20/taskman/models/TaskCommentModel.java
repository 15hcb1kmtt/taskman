package com.hcmus.group20.taskman.models;

/**
 * Created by nglekhoi on 30-Jan-17.
 */

public class TaskCommentModel {
    public String UserId;
    public String Email;
    public long Timestamp;
    public String Content;
}
