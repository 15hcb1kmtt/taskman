package com.hcmus.group20.taskman;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hcmus.group20.taskman.models.MemberModel;
import com.hcmus.group20.taskman.models.TaskStorageMetadata;

import java.util.HashMap;
import java.util.Map;

public class AddGroupFragment extends Fragment implements View.OnClickListener {

    private Button btnSave;
    private Button btnCancel;
    private EditText txtGroupName;
    private EditText txtGroupDesc;

    public AddGroupFragment() {

    }

    public static AddGroupFragment newInstance() {
        AddGroupFragment fragment = new AddGroupFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_group, container, false);

        txtGroupName = (EditText) view.findViewById(R.id.add_group_txt_group_name);
        txtGroupDesc = (EditText) view.findViewById(R.id.add_group_txt_group_desc);
        btnSave = (Button) view.findViewById(R.id.add_group_btn_save);
        btnCancel = (Button) view.findViewById(R.id.add_group_btn_cancel);

        btnSave.setOnClickListener(this);
        btnCancel.setOnClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.add_group_btn_save: {
                FirebaseAuth auth = FirebaseAuth.getInstance();
                final FirebaseUser currentUser = auth.getCurrentUser();
                final FirebaseDatabase database = FirebaseDatabase.getInstance();
                final String groupKey = database.getReference("TaskStorageMetadatas").push().getKey();
                final String groupName = txtGroupName.getText().toString();
                TaskStorageMetadata taskStorageMetadata = new TaskStorageMetadata();
                taskStorageMetadata.Name = txtGroupName.getText().toString();
                taskStorageMetadata.Description = txtGroupDesc.getText().toString();
                taskStorageMetadata.Members = new HashMap<String, String>();
                taskStorageMetadata.Members.put(currentUser.getUid(), currentUser.getEmail());
                // add group into "TaskStorageMetadatas"
                database.getReference("TaskStorageMetadatas").child(groupKey).setValue(taskStorageMetadata);

                database.getReference("Members")
                        .child(currentUser.getUid())
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot snapshot) {
                                if (snapshot.exists()) {
                                    MemberModel member = snapshot.getValue(MemberModel.class);
                                    if (member.MemberOfGroups == null ) {
                                        member.MemberOfGroups = new HashMap<String, String>();
                                    }
                                    if (member.AdminOfGroups == null ) {
                                        member.AdminOfGroups = new HashMap<String, Boolean>();
                                    }
                                    member.MemberOfGroups.put(groupKey, groupName);
                                    member.AdminOfGroups.put(groupKey, true);
                                    database.getReference("Members").child(currentUser.getUid()).setValue(member);
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
/*
                // update members group
                MemberModel member = new MemberModel();

                Map<String, Boolean> adminOfGroups = new HashMap<String, Boolean>();
                adminOfGroups.put(groupKey, true);
                Map<String, String> memberOfGroups = new HashMap<String, String>();
                memberOfGroups.put(groupKey, groupName);

                member.AdminOfGroups = adminOfGroups;
                member.MemberOfGroups = memberOfGroups;

                Map<String, Object> toFireBaseAdminOfGroup = new HashMap<>();
                Map<String, Object> toFireBaseMembersOfGroup = new HashMap<>();

                if (database.getReference("Members").child(currentUser.getUid()).child("AdminOfGroups") == null) {

                    toFireBaseAdminOfGroup.put("AdminOfGroups", member.AdminOfGroups);

                    database.getReference("Members").child(currentUser.getUid()).updateChildren(toFireBaseAdminOfGroup);
                }
                else {
                    database.getReference("Members")
                            .child(currentUser.getUid())
                            .child("AdminOfGroups")
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {
                            Map<String, Boolean> data = new HashMap<String, Boolean>();
                            data.put(groupKey, true);
                            database.getReference("Members").child(currentUser.getUid()).child("AdminOfGroups").setValue(data);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }

                if (database.getReference("Members").child(currentUser.getUid()).child("MemberOfGroups") == null) {
                    toFireBaseMembersOfGroup.put("MemberOfGroups", member.MemberOfGroups);

                    database.getReference("Members").child(currentUser.getUid()).updateChildren(toFireBaseMembersOfGroup);
                }
                else {
                    database.getReference("Members")
                            .child(currentUser.getUid())
                            .child("MemberOfGroups")
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot snapshot) {
                                    Map<String, String> data = new HashMap<String, String>();
                                    data.put(groupKey, groupName);

                                    database.getReference("Members").child(currentUser.getUid()).child("MemberOfGroups").setValue(data);
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }*/

                Toast.makeText(getActivity(), "Added Group", Toast.LENGTH_SHORT).show();
                getActivity().onBackPressed();

                break;
            }
            case R.id.add_group_btn_cancel: {
                getActivity().onBackPressed();
                break;
            }
        }
    }
}
