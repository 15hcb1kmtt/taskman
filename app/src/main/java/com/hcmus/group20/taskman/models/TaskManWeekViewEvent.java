package com.hcmus.group20.taskman.models;

import com.alamkanak.weekview.WeekViewEvent;

import java.util.Calendar;

/**
 * Created by nglekhoi on 31-Jan-17.
 */

public class TaskManWeekViewEvent extends WeekViewEvent {
    private String stringId;

    public TaskManWeekViewEvent() {
    }

    public TaskManWeekViewEvent(long id, String name, int startYear, int startMonth, int startDay, int startHour, int startMinute, int endYear, int endMonth, int endDay, int endHour, int endMinute) {
        super(id, name, startYear, startMonth, startDay, startHour, startMinute, endYear, endMonth, endDay, endHour, endMinute);
    }

    public TaskManWeekViewEvent(long id, String name, Calendar startTime, Calendar endTime) {
        super(id, name, startTime, endTime);
    }

    public String getStringId() {
        return stringId;
    }

    public void setStringId(String stringId) {
        this.stringId = stringId;
    }
}
