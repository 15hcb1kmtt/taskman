package com.hcmus.group20.taskman.models;

import java.util.Map;

/**
 * Created by nglekhoi on 30-Jan-17.
 */

public class MemberModel {
    public String Email;
    public String PersonalStorageId;
    public String FullName;
    public String Address;
    public String Location;
    public String PhoneNumber;
    public Map<String, Boolean> AdminOfGroups;
    public Map<String, String> MemberOfGroups;

    public MemberModel() {

    }
}
