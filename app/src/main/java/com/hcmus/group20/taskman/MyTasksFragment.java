package com.hcmus.group20.taskman;

import android.content.Context;
import android.content.Intent;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.alamkanak.weekview.DateTimeInterpreter;
import com.alamkanak.weekview.MonthLoader;
import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.hcmus.group20.taskman.models.MemberModel;
import com.hcmus.group20.taskman.models.TaskManWeekViewEvent;
import com.hcmus.group20.taskman.models.TaskModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

public class MyTasksFragment extends Fragment
        implements
        View.OnClickListener,
        WeekView.EventClickListener,
        MonthLoader.MonthChangeListener,
        WeekView.EventLongPressListener,
        WeekView.EmptyViewLongPressListener {

    private static final int TYPE_DAY_VIEW = 1;
    private static final int TYPE_THREE_DAY_VIEW = 2;
    private static final int TYPE_WEEK_VIEW = 3;
    private int mWeekViewType = TYPE_THREE_DAY_VIEW;
    private static final int REQUEST_CODE_EDIT_TASK = 2;
    private static final int REQUEST_CODE_NEW_TASK = 1;

    public MyTasksFragment() {

    }

    public static MyTasksFragment newInstance() {
        MyTasksFragment fragment = new MyTasksFragment();
        return fragment;
    }

    private String personalStorageId;
    private DatabaseReference mDbReference;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        FirebaseAuth auth = FirebaseAuth.getInstance();
        final FirebaseUser user = auth.getCurrentUser();

        mDbReference = FirebaseDatabase.getInstance().getReference();
        final Query queryMemberInfo = mDbReference.child("Members").orderByKey().equalTo(user.getUid());
        queryMemberInfo.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    MemberModel member = dataSnapshot.child(user.getUid()).getValue(MemberModel.class);
                    personalStorageId = member.PersonalStorageId;
                    queryMemberInfo.removeEventListener(this);
                    mWeekView.notifyDatasetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.option_menu_my_tasks, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }

    private WeekView mWeekView;
    private FloatingActionButton fabCreateTask;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_tasks, container, false);

        // Get a reference for the week view in the layout.
        mWeekView = (WeekView)view.findViewById(R.id.weekView);

        // Set an action when any event is clicked.
        mWeekView.setOnEventClickListener(this);

        // The week view has infinite scrolling horizontally. We have to provide the events of a
        // month every time the month changes on the week view.
        mWeekView.setMonthChangeListener(this);

        // Set long press listener for events.
        mWeekView.setEventLongPressListener(this);

        fabCreateTask = (FloatingActionButton)getActivity().findViewById(R.id.fab);
        fabCreateTask.setOnClickListener(this);

        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.option_nav_week_view_today:
                mWeekView.goToToday();
                return true;
            case R.id.option_nav_week_view_1_day:
                if (mWeekViewType != TYPE_DAY_VIEW) {
                    item.setChecked(!item.isChecked());
                    mWeekViewType = TYPE_DAY_VIEW;
                    mWeekView.setNumberOfVisibleDays(1);

                    // Lets change some dimensions to best fit the view.
                    mWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()));
                    mWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
                    mWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
                }
                return true;
            case R.id.option_nav_week_view_3_days:
                if (mWeekViewType != TYPE_THREE_DAY_VIEW) {
                    item.setChecked(!item.isChecked());
                    mWeekViewType = TYPE_THREE_DAY_VIEW;
                    mWeekView.setNumberOfVisibleDays(3);

                    // Lets change some dimensions to best fit the view.
                    mWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()));
                    mWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
                    mWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
                }
                return true;
            case R.id.option_nav_week_view_1_week:
                if (mWeekViewType != TYPE_WEEK_VIEW) {
                    item.setChecked(!item.isChecked());
                    mWeekViewType = TYPE_WEEK_VIEW;
                    mWeekView.setNumberOfVisibleDays(7);

                    // Lets change some dimensions to best fit the view.
                    mWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, getResources().getDisplayMetrics()));
                    mWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10, getResources().getDisplayMetrics()));
                    mWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10, getResources().getDisplayMetrics()));
                }
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.fab: {
                Intent addTaskIntent = new Intent(this.getActivity(), AddMyTaskActivity.class);
                startActivityForResult(addTaskIntent, REQUEST_CODE_NEW_TASK);
                break;
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    protected String getEventTitle(Calendar time) {
        return String.format("Event of %02d:%02d %s/%d", time.get(Calendar.HOUR_OF_DAY), time.get(Calendar.MINUTE), time.get(Calendar.MONTH)+1, time.get(Calendar.DAY_OF_MONTH));
    }

    List<WeekViewEvent> populatedEvents = new ArrayList<WeekViewEvent>();
    boolean eventsLoaded = false;

    private boolean eventMatches(WeekViewEvent event, int year, int month) {
        return (event.getStartTime().get(Calendar.YEAR) == year && event.getStartTime().get(Calendar.MONTH) == month - 1) || (event.getEndTime().get(Calendar.YEAR) == year && event.getEndTime().get(Calendar.MONTH) == month - 1);
    }

    @Override
    public List<? extends WeekViewEvent> onMonthChange(int newYear, int newMonth) {
        List<WeekViewEvent> matchedEvents = new ArrayList<WeekViewEvent>();

        if (personalStorageId == null) {
            return populatedEvents;
        }

        if (!eventsLoaded) {

            Calendar calFrom = Calendar.getInstance();
            calFrom.set(newYear, newMonth, 1);

            Calendar calTo = Calendar.getInstance();
            calTo.set(newYear, newMonth, calFrom.getActualMaximum(Calendar.DATE));

            final Query tasksQuery = mDbReference.child("TaskStorages")
                    .child(personalStorageId);
            eventsLoaded = true;

            tasksQuery.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot task : dataSnapshot.getChildren()) {
                        TaskModel taskModel = task.getValue(TaskModel.class);

                        Calendar taskFromDate = Calendar.getInstance();
                        taskFromDate.setTimeInMillis(taskModel.FromDate);

                        Calendar taskToDate = Calendar.getInstance();
                        taskToDate.setTimeInMillis(taskModel.ToDate);

                        long range = 1234567L;
                        Random r = new Random();
                        long number = (long) (r.nextDouble() * range);

                        TaskManWeekViewEvent event = new TaskManWeekViewEvent(number, taskModel.Title, taskFromDate, taskToDate);
                        event.setStringId(task.getKey());
                        event.setColor(getResources().getColor(R.color.event_color_01));
                        populatedEvents.add(event);
                    }
                    tasksQuery.removeEventListener(this);
                    mWeekView.notifyDatasetChanged();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    // Getting Post failed, log a message
                    Log.w("MyTask", "loadPost:onCancelled", databaseError.toException());
                    tasksQuery.removeEventListener(this);
                    eventsLoaded = true;
                }
            });
        } else {
            for (WeekViewEvent event : populatedEvents) {
                if (eventMatches(event, newYear, newMonth)) {
                    matchedEvents.add(event);
                }
            }
        }
        return matchedEvents;
    }

    @Override
    public void onEmptyViewLongPress(Calendar time) {

    }

    @Override
    public void onEventClick(WeekViewEvent event, RectF eventRect) {
        TaskManWeekViewEvent taskManEvent = (TaskManWeekViewEvent)event;
        String taskId = taskManEvent.getStringId();

        Intent editTaskIntent = new Intent(this.getActivity(), EditMyTaskActivity.class);
        editTaskIntent.putExtra("StorageId", personalStorageId);
        editTaskIntent.putExtra("TaskId", taskId);
        startActivityForResult(editTaskIntent, REQUEST_CODE_EDIT_TASK);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_NEW_TASK:
            {
                if (resultCode == AppCompatActivity.RESULT_OK) {
                    populatedEvents = new ArrayList<WeekViewEvent>();
                    eventsLoaded = false;
                    mWeekView.notifyDatasetChanged();
                }
                break;
            }
            case REQUEST_CODE_EDIT_TASK:
            {
                if (resultCode == AppCompatActivity.RESULT_OK) {
                    populatedEvents = new ArrayList<WeekViewEvent>();
                    eventsLoaded = false;
                    mWeekView.notifyDatasetChanged();
                }
                break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onEventLongPress(WeekViewEvent event, RectF eventRect) {

    }
}
