package com.hcmus.group20.taskman;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;

public class ChangePasswordFragment extends Fragment implements View.OnClickListener {
    private EditText txtCurPassword;
    private EditText txtNewPassword;
    private EditText txtConfirmPassword;
    private Button btnSave;
    private Button btnCancel;

    public ChangePasswordFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static ChangePasswordFragment newInstance() {
        ChangePasswordFragment fragment = new ChangePasswordFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_change_password, container, false);

        txtCurPassword = (EditText) view.findViewById(R.id.change_password_txt_cur_password);
        txtNewPassword = (EditText) view.findViewById(R.id.change_password_txt_new_password);
        txtConfirmPassword = (EditText) view.findViewById(R.id.change_password_txt_confirm_password);
        btnSave = (Button) view.findViewById(R.id.change_password_btn_save);
        btnCancel = (Button) view.findViewById(R.id.change_password_btn_cancel);

        btnSave.setOnClickListener(this);
        btnCancel.setOnClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void backToAccountInfo() {
        Fragment fragment = AccountInfoFragment.newInstance();
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.taskman_frame_content, fragment);
        ft.commit();

        getActivity().setTitle("Thông tin tài khoản");
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.change_password_btn_save: {
                handleChangePassword();

                break;
            }
            case R.id.change_password_btn_cancel: {
                backToAccountInfo();
                break;
            }
        }
    }

    private void handleChangePassword() {
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        AuthCredential credential = EmailAuthProvider
                .getCredential(user.getEmail(), txtCurPassword.getText().toString());

        user.reauthenticate(credential)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (!task.isSuccessful()) {
                            try {
                                throw task.getException();
                            } catch(FirebaseAuthInvalidCredentialsException e) {
                                Toast.makeText(getActivity(), "Invalid Credential", Toast.LENGTH_SHORT).show();
                            } catch(Exception e) {
                                Log.e("Error:", e.getMessage());
                            }
                        }
                        else {
                            String newPass = txtNewPassword.getText().toString();
                            String confirmPass = txtConfirmPassword.getText().toString();
                            if (newPass.equals(confirmPass)) {
                                user.updatePassword(newPass);
                                Toast.makeText(getActivity(), "Updated Successfully", Toast.LENGTH_SHORT).show();
                                backToAccountInfo();
                            }
                            else {
                                Toast.makeText(getActivity(), "Password doesn't match", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
    }
}
