package com.hcmus.group20.taskman;

/**
 * Created by Trung-NM on 1/24/2017.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.hcmus.group20.taskman.models.GroupMemberModel;

import java.util.ArrayList;
import java.util.List;

public class GroupMemberDataAdapter extends RecyclerView.Adapter {
    private List<GroupMemberModel> members;
    private Context context;
    private String loggedInUserId;
    private Boolean isAdmin;

    public GroupMemberDataAdapter(Context context, List<GroupMemberModel> members, String loggedInUserId, Boolean isAdmin) {
        this.context = context;
        this.members = members;
        this.loggedInUserId = loggedInUserId;
        this.isAdmin = isAdmin;
    }

    @Override
    public GroupMemberDataAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final GroupMemberModel member = members.get(position);
        ((ViewHolder)holder).txtEmail.setText(member.Email);
        if (isAdmin) {
            if (loggedInUserId.equals(member.Id)) {
                ((ViewHolder)holder).btnDelete.setVisibility(View.INVISIBLE);
            } else {
                ((ViewHolder)holder).btnDelete.setVisibility(View.VISIBLE);
                ((ViewHolder)holder).btnDelete.setOnClickListener(new Button.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (context instanceof GroupDetailActivity) {
                            ((GroupDetailActivity) context).RemoveMember(member.Id);
                        }
                    }
                });
            }
        } else {
            ((ViewHolder)holder).btnDelete.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return members.size();
    }

    public void removeItem(int position) {
        members.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, members.size());
    }
    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView txtEmail;
        public Button btnDelete;

        public ViewHolder(View view) {
            super(view);
            txtEmail = (TextView)view.findViewById(R.id.group_member_txt_email);
            btnDelete = (Button)view.findViewById(R.id.group_member_btn_delete);
        }
    }
}