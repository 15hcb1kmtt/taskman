package com.hcmus.group20.taskman;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.Pair;
import android.view.SubMenu;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.hcmus.group20.taskman.models.MemberModel;

import java.util.HashMap;
import java.util.Map;

public class TaskManActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener
{

    private FloatingActionButton fabCreateTask;
    private static final int NAV_GROUP_SELECTED_ID = 7373;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_man);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fabCreateTask = (FloatingActionButton) findViewById(R.id.fab);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //final MenuItem menu = navigationView.getMenu().findItem(R.id.nav_group_task);
        //SubMenu subMenu = menu.getSubMenu();
        //subMenu.add(R.id.nav_my_groups_group, 1, 101, "Gorpu").setIcon(android.R.drawable.ic_menu_agenda);

        LoadUserGroups();

        showFragment(R.id.nav_my_task);
    }

    private DatabaseReference mDbReference;
    private NavigationView navigationView;
    private Map<MenuItem, String> groupMenuItems;

    private void LoadUserGroups()
    {
        final MenuItem menu = navigationView.getMenu().findItem(R.id.nav_group_task);
        FirebaseAuth auth = FirebaseAuth.getInstance();
        final FirebaseUser user = auth.getCurrentUser();
        mDbReference = FirebaseDatabase.getInstance().getReference();
        final Query queryMemberInfo = mDbReference.child("Members").child(user.getUid());
        queryMemberInfo.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    groupMenuItems = new HashMap<MenuItem, String>();
                    MemberModel member = dataSnapshot.getValue(MemberModel.class);
                    if (member.MemberOfGroups != null) {
                        for (Map.Entry<String, String> entry : member.MemberOfGroups.entrySet()) {
                            String key = entry.getKey();
                            String value = entry.getValue();
                            SubMenu subMenu = menu.getSubMenu();
                            MenuItem item = subMenu.add(R.id.nav_my_groups_group, NAV_GROUP_SELECTED_ID, 101, value).setIcon(android.R.drawable.ic_menu_agenda);
                            groupMenuItems.put(item, key);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
    }

    public Pair<String, String> getSelectedGroupData(MenuItem item)
    {
        Pair<String, String> ret = new Pair<String, String>(groupMenuItems.get(item), item.getTitle().toString());
        return ret;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.option_menu_task_man, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id != NAV_GROUP_SELECTED_ID) {
            showFragment(id);
        } else {
            Pair<String, String> groupInfo = getSelectedGroupData(item);
            showGroupTaskFragment(groupInfo.first, groupInfo.second);
        }
        return true;
    }

    private void showGroupTaskFragment(String groupId, String groupName) {
        Fragment fragment = null;
        String title = getString(R.string.app_name);

        title = groupName;
        fragment = GroupTasksFragment.newInstance(groupId, groupName);

        fabCreateTask.setVisibility(View.VISIBLE);

        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.taskman_frame_content, fragment);
            ft.commit();
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    private void showFragment(int id) {

        Fragment fragment = null;
        String title = getString(R.string.app_name);
        boolean showFab = false;

        switch (id)
        {
            case R.id.nav_my_task:
            {
                title = "Công việc của tôi";
                fragment = MyTasksFragment.newInstance();
                showFab = true;
                break;
            }
            case R.id.nav_add_group:
            {
                title = "Thêm nhóm";
                fragment = AddGroupFragment.newInstance();
                break;
            }
            case R.id.nav_account_info:
            {
                title = "Thông tin tài khoản";
                fragment = AccountInfoFragment.newInstance();
                break;
            }
            case R.id.nav_signout:
            {
                FirebaseAuth.getInstance().signOut();
                Intent loginIntent = new Intent(this, LoginActivity.class);
                startActivity(loginIntent);
                finishActivity(0);
                break;
            }
        }

        if (showFab) {
            fabCreateTask.setVisibility(View.VISIBLE);
        } else {
            fabCreateTask.setVisibility(View.INVISIBLE);
        }

        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.taskman_frame_content, fragment);
            ft.commit();
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }
}
