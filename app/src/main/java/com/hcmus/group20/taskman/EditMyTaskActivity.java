package com.hcmus.group20.taskman;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.hcmus.group20.taskman.models.MemberModel;
import com.hcmus.group20.taskman.models.TaskModel;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditMyTaskActivity extends AppCompatActivity
    implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener
{

    private Calendar fromDate;
    private Calendar toDate;

    @BindView(R.id.add_my_task_txt_fromDate)
    public EditText txt_fromDate;

    @BindView(R.id.add_my_task_txt_toDate)
    public EditText txt_toDate;

    @BindView(R.id.add_my_task_txt_title)
    public EditText txt_title;

    @BindView(R.id.add_my_task_txt_content)
    public EditText txt_content;

    @BindView(R.id.add_my_task_spn_priority)
    public Spinner spn_priority;

    @BindView(R.id.add_my_task_spn_status)
    public Spinner spn_status;

    private DatabaseReference mDatabase;
    private ArrayAdapter<CharSequence> priorityAdapter;
    private ArrayAdapter<CharSequence> statusAdapter;
    private String storageId;
    private String taskId;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.option_menu_edit_my_task, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.option_nav_delete_task:
            {
                mDatabase.child("TaskStorages").child(storageId).child(taskId).removeValue();
                this.setResult(RESULT_OK);
                this.finish();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_my_task);
        ButterKnife.bind(this);

        priorityAdapter = ArrayAdapter.createFromResource(this, R.array.task_priority_array, android.R.layout.simple_spinner_item);
        priorityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_priority.setAdapter(priorityAdapter);

        statusAdapter = ArrayAdapter.createFromResource(this, R.array.task_status_array, android.R.layout.simple_spinner_item);
        statusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spn_status.setAdapter(statusAdapter);

        taskId = this.getIntent().getStringExtra("TaskId");
        storageId = this.getIntent().getStringExtra("StorageId");
        mDatabase = FirebaseDatabase.getInstance().getReference();
        final Query queryMemberInfo = mDatabase.child("TaskStorages").child(storageId).child(taskId);
        queryMemberInfo.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    TaskModel task = dataSnapshot.getValue(TaskModel.class);
                    BindTask(task);
                    queryMemberInfo.removeEventListener(this);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
    }

    public void BindTask(TaskModel model) {
        fromDate = Calendar.getInstance();
        fromDate.setTimeInMillis(model.FromDate);

        toDate = Calendar.getInstance();
        toDate.setTimeInMillis(model.ToDate);

        txt_fromDate.setText(fromDate.getTime().toString());
        txt_toDate.setText(toDate.getTime().toString());

        txt_title.setText(model.Title);
        txt_content.setText(model.Description);

        spn_priority.setSelection(priorityAdapter.getPosition(model.Priority));
        spn_status.setSelection(statusAdapter.getPosition(model.Status));
    }

    @OnClick(R.id.add_my_task_txt_fromDate)
    public void OnFromDateClick() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getFragmentManager(), "FromDate");
    }

    @OnClick(R.id.add_my_task_txt_toDate)
    public void OnToDateClick() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getFragmentManager(), "ToDate");
    }

    @OnClick(R.id.add_my_task_btn_save)
    public void OnSaveClick() {
        // Get Member Data
        FirebaseAuth auth = FirebaseAuth.getInstance();
        final FirebaseUser user = auth.getCurrentUser();
        final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        Query mQueryRef = mDatabase.child("Members").orderByKey().equalTo(user.getUid());
        mQueryRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    TaskModel taskModel = new TaskModel();
                    taskModel.AssignToEmail = user.getEmail();
                    taskModel.AssignToId = user.getUid();
                    taskModel.CreatedByEmail = user.getEmail();
                    taskModel.CreatedById = user.getUid();
                    taskModel.Description = txt_content.getText().toString();
                    taskModel.FromDate = fromDate.getTimeInMillis();
                    taskModel.ToDate = toDate.getTimeInMillis();
                    taskModel.Priority = spn_priority.getSelectedItem().toString();
                    taskModel.Status = spn_status.getSelectedItem().toString();
                    taskModel.Title = txt_title.getText().toString();

                    mDatabase.child("TaskStorages").child(storageId).child(taskId).setValue(taskModel);
                    EditMyTaskActivity.this.setResult(RESULT_OK);
                    EditMyTaskActivity.this.finish();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
    }

    @OnClick(R.id.add_my_task_btn_cancel)
    public void OnCancelClick() {
        this.setResult(RESULT_CANCELED);
        this.finish();
        return;
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String tag = view.getTag();
        switch (tag) {
            case "FromDate" :
            {
                if (fromDate == null) {
                    fromDate = Calendar.getInstance();
                }
                fromDate.set(year, monthOfYear, dayOfMonth);
                break;
            }
            case "ToDate" :
            {
                if (toDate == null) {
                    toDate = Calendar.getInstance();
                }
                toDate.set(year, monthOfYear, dayOfMonth);
                break;
            }
        }
        Calendar now = Calendar.getInstance();
        TimePickerDialog dpd = TimePickerDialog.newInstance(
                this,
                now.get(Calendar.HOUR),
                now.get(Calendar.MINUTE),
                true
        );
        dpd.show(getFragmentManager(), view.getTag());
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        String tag = view.getTag();
        switch (tag) {
            case "FromDate" :
            {
                fromDate.set(
                        fromDate.get(Calendar.YEAR),
                        fromDate.get(Calendar.MONTH),
                        fromDate.get(Calendar.DAY_OF_MONTH),
                        hourOfDay,
                        minute,
                        second);
                txt_fromDate.setText(fromDate.getTime().toString());
                break;
            }
            case "ToDate" :
            {
                toDate.set(
                        toDate.get(Calendar.YEAR),
                        toDate.get(Calendar.MONTH),
                        toDate.get(Calendar.DAY_OF_MONTH),
                        hourOfDay,
                        minute,
                        second);
                txt_toDate.setText(toDate.getTime().toString());
                break;
            }
        }
    }
}
