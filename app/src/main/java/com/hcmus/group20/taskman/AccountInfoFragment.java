package com.hcmus.group20.taskman;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hcmus.group20.taskman.models.MemberModel;
import com.hcmus.group20.taskman.models.TaskModel;

import java.util.HashMap;
import java.util.Map;

public class AccountInfoFragment extends Fragment implements View.OnClickListener {

    private EditText txtFullName;
    private EditText txtAddress;
    private EditText txtEmail;
    private EditText txtPhoneNumber;
    private Button btnSave;
    private Button btnCancel;
    private Button btnChangePassword;
    private Spinner spinnerLocation;

    public AccountInfoFragment() {
        // Required empty public constructor
    }

    public static AccountInfoFragment newInstance() {
        AccountInfoFragment fragment = new AccountInfoFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_account_info, container, false);

        txtFullName = (EditText) view.findViewById(R.id.account_info_txt_full_name);
        txtAddress = (EditText) view.findViewById(R.id.account_info_txt_address);
        txtEmail = (EditText) view.findViewById(R.id.account_info_txt_email);
        txtEmail.setEnabled(false);
        txtPhoneNumber = (EditText) view.findViewById(R.id.account_info_txt_phone);
        spinnerLocation = (Spinner) view.findViewById(R.id.account_info_spinner_location);
        btnSave = (Button) view.findViewById(R.id.account_info_btn_save);
        btnCancel = (Button) view.findViewById(R.id.account_info_btn_cancel);
        btnChangePassword = (Button) view.findViewById(R.id.account_info_btn_change_password);

        btnSave.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        btnChangePassword.setOnClickListener(this);

        FirebaseAuth auth = FirebaseAuth.getInstance();
        final FirebaseUser currentUser = auth.getCurrentUser();
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        txtEmail.setText(currentUser.getEmail().toString());

        // load values
        database.getReference("Members")
                .orderByKey().equalTo(currentUser.getUid())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            MemberModel member = dataSnapshot.child(currentUser.getUid()).getValue(MemberModel.class);
                            setFormValues(member);
                            return;
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.account_info_btn_save: {
                FirebaseAuth auth = FirebaseAuth.getInstance();
                final FirebaseUser currentUser = auth.getCurrentUser();
                final FirebaseDatabase database = FirebaseDatabase.getInstance();
                Map<String, Object> childUpdates = new HashMap<>();
                childUpdates.put( "FullName", txtFullName.getText().toString() );
                childUpdates.put( "Address", txtAddress.getText().toString() );
                childUpdates.put( "PhoneNumber", txtPhoneNumber.getText().toString() );
                childUpdates.put( "Location", spinnerLocation.getSelectedItem().toString() );
                database.getReference("Members").child(currentUser.getUid()).updateChildren(childUpdates);

                Toast.makeText(getActivity(), "Updated Successfully", Toast.LENGTH_SHORT).show();
                getActivity().onBackPressed();

                break;
            }
            case R.id.account_info_btn_cancel: {
                getActivity().onBackPressed();
                break;
            }
            case R.id.account_info_btn_change_password: {
                getActivity().setTitle("Đổi password");
                Fragment fragment = ChangePasswordFragment.newInstance();
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.taskman_frame_content, fragment);
                ft.commit();


            }
        }
    }

    public void setFormValues(MemberModel member) {
        txtFullName.setText(member.FullName);
        txtAddress.setText(member.Address);
        txtPhoneNumber.setText(member.PhoneNumber);
        spinnerLocation.setSelection(getSelectedIndex(spinnerLocation, member.Location));
    }

    private int getSelectedIndex(Spinner spinner, String value)
    {
        int index = 0;

        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(value)){
                index = i;
                break;
            }
        }

        return index;
    }
}
