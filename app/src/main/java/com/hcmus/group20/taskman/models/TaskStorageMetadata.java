package com.hcmus.group20.taskman.models;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by nglekhoi on 30-Jan-17.
 */

public class TaskStorageMetadata {
    public TaskStorageMetadata() {

    }

    public String Name;
    public String Description;
    public Map<String, String> Members;
}
