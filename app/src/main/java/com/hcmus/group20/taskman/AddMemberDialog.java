package com.hcmus.group20.taskman;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hcmus.group20.taskman.models.MemberModel;
import com.hcmus.group20.taskman.models.TaskStorageMetadata;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddMemberDialog extends DialogFragment {

    public AddMemberDialog() {
        // Required empty public constructor
    }

    public static AddMemberDialog newInstance(String title, String storageId, String groupName) {
        AddMemberDialog fragment = new AddMemberDialog();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("storageId", storageId);
        args.putString("groupName", groupName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_member_dialog, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @BindView(R.id.dialog_add_member_txt_email)
    public EditText txtEmail;

    @OnClick(R.id.dialog_add_member_btn_ok)
    public void AddMember() {
        final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("Members")
                .orderByChild("Email")
                .equalTo(txtEmail.getText().toString())
                .limitToFirst(1)
                .addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot snapShot: dataSnapshot.getChildren()) {
                        final MemberModel member = snapShot.getValue(MemberModel.class);
                        final String memberKey = snapShot.getKey();
                        if (member.MemberOfGroups != null) {
                            if (!member.MemberOfGroups.containsKey(storageId)) {
                                AddMemberToDatabase(member, memberKey, mDatabase);
                                AddMemberDialog.this.dismiss();
                            }
                        } else {
                            member.MemberOfGroups = new HashMap<String, String>();
                            AddMemberToDatabase(member, memberKey, mDatabase);
                            AddMemberDialog.this.dismiss();
                        }
                    }
                } else {
                    Toast.makeText(getActivity(), "Không tìm thấy thành viên này.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
        this.dismiss();
    }

    private void AddMemberToDatabase(final MemberModel member, final String memberKey, final DatabaseReference mDatabase) {
        member.MemberOfGroups.put(storageId, groupName);
        mDatabase.child("TaskStorageMetadatas")
                .child(storageId)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        TaskStorageMetadata meta = dataSnapshot.getValue(TaskStorageMetadata.class);
                        if (meta.Members == null) {
                            meta.Members = new HashMap<String, String>();
                        }
                        if (!meta.Members.containsKey(memberKey)) {
                            meta.Members.put(memberKey, member.Email);
                        }
                        mDatabase.child("TaskStorageMetadatas").child(storageId).setValue(meta);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                }
        );
        mDatabase.child("Members").child(memberKey).setValue(member);
    }

    @OnClick(R.id.dialog_add_member_btn_cancel)
    public void CancelAddMember() {
        this.dismiss();
    }

    private String storageId;
    private String groupName;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String title = getArguments().getString("title", "Vui Lòng Nhập Email");
        storageId = getArguments().getString("storageId");
        groupName = getArguments().getString("groupName");
        getDialog().setTitle(title);
        txtEmail.requestFocus();
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
